const Cell = require("angstloch").Cell;
const path = require("path");
const Jimp = require("jimp");

const TILES = {};
const PROMISES = [
	Jimp.read(path.join(__dirname, "../tiles/floor.png")),
	Jimp.read(path.join(__dirname, "../tiles/wall.png")),
	Jimp.read(path.join(__dirname, "../tiles/door.png"))
];

class Image {
	generate(map, filename) {
		let image = new Jimp(map.width * 8, map.height * 8);

		Promise.all(PROMISES)
			.then(result => {
				TILES[Cell.TYPE_FLOOR] = result[0];
				TILES[Cell.TYPE_WALL] = result[1];
				TILES[Cell.TYPE_DOOR] = result[2];
			})
			.then(() => {
				map.forEachCell((x, y, cell) => {
					image.blit(TILES[cell.type], x * 8, y * 8);
				});
			})
			.then(() => {
				image.write(filename);
			});
	}
}

module.exports.Image = Image;
